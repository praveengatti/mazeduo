﻿using GameSparks.Api.Responses;
using Khadga.Fusion;
using Khadga.Leaderboard;
using Khadga.Utility.Time;
using System.Collections.Generic;

namespace Khadga.GSBaas
{

    public class GSLeaderboardService : ILeaderboardService
    {
        [Inject]
        public LeaderboardResponseSignal LeaderboardResponseSignal { get; set; }

        [Inject]
        public MultiplayerLeaderboarResponseSignal MultiplayerLeaderboarResponseSignal { get; set; }

        public void SubmitForLeaderboard(long timeTaken)
        {
            /* new GameSparks.Api.Requests.LogEventRequest()
           .SetEventKey("SubmitScore")
           .SetEventAttribute("Score", timeTaken)
           .Send((response) =>
           {
           }); */
        }

        public void SubmitMultiplayerLeaderboard(float timeTaken, string pair)
        {
            new GameSparks.Api.Requests.LogEventRequest()
         .SetEventKey("SubmitToMPLB")
         .SetEventAttribute("TimeTaken", (long)timeTaken)
         .SetEventAttribute("Pair",pair)
         .Send((response) =>
         {
         });
        }


        public void RequestLeaderboard()
        {
            new GameSparks.Api.Requests.LeaderboardDataRequest()
           .SetLeaderboardShortCode("BestTimeLeaderboard")
           .SetEntryCount(10) // we need to parse this text input, since the entry count only takes long
           .Send((response) =>
           {
               List<LeaderboardItem> LBItemList = new List<LeaderboardItem>();
               if (!response.HasErrors)
                   foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                   {
                       LeaderboardItem lb = new LeaderboardItem();
                       lb.Name = entry.UserName;
                       lb.Rank = (int)entry.Rank;
                       long bs = (long)entry.GetNumberValue("MIN-BestScore");
                       float bt = bs / 1000;
                       lb.DecisionAttribute1 = TimeFormatUtil.SecondToHHMMSSmm((bt));
                       LBItemList.Add(lb);
                   }
               LeaderboardResponseSignal.Dispatch(LBItemList);
           }
           );
        }

        public void RequestMultiplayerLeaderboard()
        {
            new GameSparks.Api.Requests.LeaderboardDataRequest()
           .SetLeaderboardShortCode("MultiplayerLeaderboard")
           .SetEntryCount(10) // we need to parse this text input, since the entry count only takes long
           .Send((response) =>
           {
               List<LeaderboardItem> LBItemList = new List<LeaderboardItem>();
               if (!response.HasErrors)
                   foreach (GameSparks.Api.Responses.LeaderboardDataResponse._LeaderboardData entry in response.Data) // iterate through the leaderboard data
                   {
                       LeaderboardItem lb = new LeaderboardItem();
                       lb.Name = entry.GetStringValue("SUPPLEMENTAL-Pair");
                       lb.Rank = (int)entry.Rank;
                       long bs = (long)entry.GetNumberValue("MIN-TimeTaken");
                       float bt = bs / 1000;
                       lb.DecisionAttribute1 = ((bt).ToString()+"s");
                       LBItemList.Add(lb);
                   }
               MultiplayerLeaderboarResponseSignal.Dispatch(LBItemList);
           }
           );
        }
    }
}








