﻿using GameSparks.Api.Messages;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using GameSparks.Api.Requests;
using GameSparks.Core;
using Khadga.Fusion;
using Khadga.Fusion.Model;
using Newtonsoft.Json;
using Khadga.Utility.LocalData;
using Khadga.Utility;

namespace Khadga.GSBaas
{
    public class GSRealTimeService
    {
        [Inject]
        public ChallengeIssuedSignal ChallengeIssuedSignal { get; set; }

        [Inject]
        public ChallengeStartedSignal ChallengeStartedSignal { get; set; }

        [Inject]
        public ChallengeDetails ChallengeDetails { get; set; }



        // get challenged maze in challenge started message to both players from gs, currently patching the message to a default maze string


        public void InitializeChallengeListerners()
        {
            ChallengeIssuedMessage.Listener += ChallengeIssuedMessageHandler;
            ChallengeStartedMessage.Listener += ChallengeStartedMessageHandler;
            ChallengeAcceptedMessage.Listener += ChallengeAcceptedMessageHandler;
            ChallengeJoinedMessage.Listener += ChallengeJoinedMessageHandler;
        }

        private void ChallengeJoinedMessageHandler(ChallengeJoinedMessage obj)
        {
            Debug.Log("maze duo custom call: challenge joined");

        }

        private void ChallengeAcceptedMessageHandler(ChallengeAcceptedMessage obj)
        {
            Debug.Log("maze duo custom call: challenge accepted");
        }

        private void ChallengeStartedMessageHandler(ChallengeStartedMessage obj)
        {
            //throw new NotImplementedException();
            // Challenge is accepted here handle here
            GSData matchDetails = obj.ScriptData.GetGSData("MatchDetails");
            int portID = (int)matchDetails.GetInt("Port");
            string hostURL = matchDetails.GetString("Host");
            var Participants = matchDetails.GetGSDataList("Participants");

            ChallengeDetails.ChallengeID = matchDetails.GetString("MatchId");

            var opponentPlayer = Participants.Find(player => player.GetGSData("ExternalIds").GetString("FB") != ChallengeDetails.UserFacebookID);
            ChallengeDetails.OpponentAccessToken = opponentPlayer.GetString("AccessToken");
            ChallengeDetails.OpponentFacebookID = opponentPlayer.GetGSData("ExternalIds").GetString("FB");
            ChallengeDetails.OpponentName = opponentPlayer.GetString("DisplayName");
            ChallengeDetails.OpponnetPeerID = (int)opponentPlayer.GetInt("PeerId");

            var currentPlayer = Participants.Find(player => player.GetGSData("ExternalIds").GetString("FB") == ChallengeDetails.UserFacebookID);
            string accessToken = currentPlayer.GetString("AccessToken");
            string matchID = matchDetails.GetString("MatchId");
            ChallengeDetails.UserPeerID = (int)currentPlayer.GetInt("PeerId");
            RTSessionInfo rtsinfo = new RTSessionInfo(portID,hostURL,accessToken ,matchID);

            GSRTManager.Instance().StartNewRTSession(rtsinfo);



            Debug.LogWarning(obj.ScriptData.JSON.ToString());

        }

        private void ChallengeIssuedMessageHandler(ChallengeIssuedMessage cm)
        {
            ChallengeDetails cd = new ChallengeDetails();
            cd.UserFacebookID = cm.Challenge.Challenger.ExternalIds.GetString("FB");
            cd.ChallengeID = cm.Challenge.ChallengeId;
            ChallengeDetails.ChallengeMaze = JsonConvert.DeserializeObject<int[,]>(cm.Challenge.ChallengeMessage);
            ChallengeIssuedSignal.Dispatch(cd);
        }

        public void AcceptChallenge(string challengeId)
        {
            new AcceptChallengeRequest()
        .SetChallengeInstanceId(challengeId)
        .SetMessage("Done")
        .Send((response) =>
        {
            string challengeInstanceId = response.ChallengeInstanceId;
        });

        }

        public void CreateMatch(string Player1ID, string Player2ID)
        {
            new LogEventRequest()
            .SetEventKey("FBFriendBattle")
            .SetEventAttribute("Player1ID", Player1ID)
            .SetEventAttribute("Player2ID", Player2ID)
            .Send((response) =>
            {
                Debug.Log(response.JSONData);
                Debug.Log(Player1ID);
                Debug.Log(Player2ID);
                Debug.Log(response.ScriptData.GetString("MatchID"));
                Debug.Log(response.ScriptData.GetString("p1Name"));
                Debug.Log(response.ScriptData.GetString("p2Name"));
                Debug.Log(response.ScriptData.GetString("p1AccessToken"));
                Debug.Log(response.ScriptData.GetString("p2AccessToken"));
            });
        }


        public void ChallengeFriends(List<string> friendFBIds)
        {

            new CreateChallengeRequest().SetChallengeShortCode("FBFriendChallenge")
                    .SetAccessType("PRIVATE")
                    .SetUsersToChallenge(friendFBIds) //We supply the userIds of who we wish to challenge
                    .SetEndTime(System.DateTime.Today.AddDays(1)) //We set a date and time the challenge will end on
                    .SetChallengeMessage(JsonConvert.SerializeObject(GenerateRandomMaze(Configuration.MazeWidth, Configuration.MazeHeight))) // We can send a message along with the invite, we are sending a random maze to other player
                    .Send((response) =>
                    {
                        if (response.HasErrors)
                        {
                            Debug.Log(response.Errors);
                            Debug.LogError(response.Errors.JSON);
                        }
                        else
                        {
                            //Show message saying sent!;
                        }
                    });
        }

        public int[,] GenerateRandomMaze(int width, int height)
        {
            return ChallengeDetails.ChallengeMaze = MazeUtil.CreateMaze(width, height);
        }

        public int[,] GenerateStaticMaze(int i, int j)
        {
            int[,] maze =
                 {{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},{1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},{1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1},{1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1},{1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1},{1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},{1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1},{1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1},{1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1},{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},{1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},{1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1},{1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1},{1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 1},{1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1},{1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1},{1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1},{1, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},{1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1},{1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1},{1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 0, 1},{1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},{1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1},{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1},{1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},{1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1},{1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1},{1, 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1},{1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1},{1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 1},{1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1},{1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1},{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}};
            return maze;
        }


    }





}








