﻿using Khadga.Authentication;
using System.Collections;
using System.Collections.Generic;
using Khadga.Fusion.Model;
using UnityEngine;
using System;
using GameSparks.Core;
using GameSparks.Api.Responses;
using Khadga.Fusion;
using Khadga.Shared;
using Facebook.Unity;
using UnityEngine.UI;
using strange.extensions.injector.impl;
using Khadga.GSBaas;

namespace Khadga.GSBass
{
    public class GSAuthentication : IAuthenticationService
    {

        [Inject]
        public LoadPlayerSignal LoadPlayerSignal { get; set; }

        [Inject]
        public GetPlayerSignal GetPlayerSignal { get; set; }

        [Inject]
        public FBLoggedInSignal FBLoggedInSignal { get; set; }

        [Inject]
        public FBAuthenticatedSignal FBAuthenticatedSignal { get; set; }

        [Inject]
        public FBFriendsListResponseSignal FBFriendsListResponseSignal { get; set; }

        [Inject]
        public ChallengeDetails ChallengeDetails { get; set; }


        private GameSparksUnity _GameSparksUnity;

        public bool IsAuthenticated { get; private set; }

        public void TryToLogin(Credentials credentials)
        {
            throw new NotImplementedException();
        }

        public void TryToRegister(Credentials credentials)
        {
            throw new NotImplementedException();
        }

        public void InitializeAndAuthenticate()
        {
            InitializeGS();
            GS.GameSparksAvailable += OnGameSparksIsInitializedAndAvailable;
        }


        private void InitializeGS()
        {
            IsAuthenticated = false;
            if (_GameSparksUnity == null)
            {
                // Avoid duplication
                _GameSparksUnity = GameObject.FindObjectOfType<GameSparksUnity>();

                if (_GameSparksUnity == null)
                {
                    _GameSparksUnity = new GameObject(typeof(GameSparksUnity).ToString()).AddComponent<GameSparksUnity>();
                    MonoBehaviour.DontDestroyOnLoad(_GameSparksUnity);
                    _GameSparksUnity.settings = GameSparksSettings.Instance;
                }
            }
        }

        private void OnGameSparksIsInitializedAndAvailable(bool available)
        {
            try
            {
                if (available)
                {
                    if (GS.Authenticated == false)
                    {
                        var deviceAuthRequestData = new GSRequestData();
                        var deviceAuthRequest = new GameSparks.Api.Requests.DeviceAuthenticationRequest();
                        deviceAuthRequest.SetScriptData(deviceAuthRequestData);
                        deviceAuthRequest.SetDurable(true);
                        deviceAuthRequest.Send(HandleDeviceAuthResponse);
                    }
                    else
                    {
                        //It was not known yet wether it has sucessfully logged in by now or now, based on the saved token, etc.
                        //Hence we fire the signal SuccessfullyLoggedinSignal to let all the waiting parties know about it.

                        if (IsAuthenticated == false)
                        {
                            IsAuthenticated = true;
                            GetPlayerSignal.Dispatch();
                        }
                    }

                }
            }
            catch (Exception e)
            {
                Debug.Log(e.ToString());
            }
        }

        public bool IsBaasAvailable()
        {
            return GS.Available;
        }

        private void HandleDeviceAuthResponse(AuthenticationResponse response)
        {
            if (response.HasErrors)
            {
                IsAuthenticated = false;
                Debug.LogError("Device Authentication failed.");
            }
            else
            {

                IsAuthenticated = true;

                GetPlayerSignal.Dispatch();

                Debug.Log("Device Authentication Response :" + response);
            }
        }

        #region FACEBOOK


        public void InitializeFBConnectivity()
        {
            FB.Init(OnInitComplete, OnHideUnity);
            Debug.Log("maze duo custom call:  facebook init called");
        }

        private void OnHideUnity(bool isUnityShown)
        {

        }

        private void OnInitComplete()
        { 
            FB.LogInWithReadPermissions(new List<string>() { "public_profile", "email", "user_friends" }, AuthCallBack);
            Debug.Log("maze duo custom call: init completed");
        }

        private void AuthCallBack(ILoginResult result)
        {
            LoginWithFB();
            Debug.Log("maze duo custom call: auth callback completed");
        }

        private void LoginWithFB()
        {

            if (FB.IsLoggedIn)
            {
                // facebook connect request to the gamesparks
                new GameSparks.Api.Requests.FacebookConnectRequest().SetDurable(true).
                    SetDoNotLinkToCurrentPlayer(false).
                    SetSyncDisplayName(true).
                    SetAccessToken(AccessToken.CurrentAccessToken.TokenString).Send(HandleFacebookConnectResponse);
            }
            else
            {
               
                // handle user didn't click okay
            }
        }

        private void HandleFacebookConnectResponse(AuthenticationResponse response)
        {
            if (response.HasErrors)
            {
                // handle user didn't connect with fb
                foreach (var error in response.Errors.BaseData)
                {
                    Debug.Log(error.ToString());
                }
            }
            else
            {
                FBAuthenticatedSignal.Dispatch();
                GetAccoutDetails();
            }

        }

        public void GetAccoutDetails()
        {
            new GameSparks.Api.Requests.AccountDetailsRequest()
                .Send((response) =>
            {
                if (response.HasErrors)
                {
                }
                else
                {
                    if (response.ExternalIds.GetString("FB") != null)
                    {
                        FBProfile fbp = new FBProfile();
                        fbp.UserName = response.DisplayName;
                        fbp.FacebookID = response.ExternalIds.GetString("FB");
                        FBLoggedInSignal.Dispatch(fbp);

                        ChallengeDetails.UserName = fbp.UserName;
                        ChallengeDetails.UserFacebookID = fbp.FacebookID;


                        ListGameFriends();

                    }
                }
            });
        }

        public void ListGameFriends()
        {

            new GameSparks.Api.Requests.ListGameFriendsRequest()
                .Send((response) =>
            {
                List<FBProfile> fbFriends = new List<FBProfile>();
                foreach (var friend in response.Friends)
                {
                    FBProfile fbFriend = new FBProfile();
                    fbFriend.UserName = friend.DisplayName;
                    fbFriend.FacebookID = friend.ExternalIds.GetString("FB");
                    fbFriend.GSID = friend.Id;
                    // fbFriend.Best = add personal best of friend here
                    fbFriends.Add(fbFriend);
                }
                FBFriendsListResponseSignal.Dispatch(fbFriends);
            }
            );
        }

        #endregion
    }
}
