﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Khadga.Fusion.Controller
{
    public class StartMenuCommand : Command
    {
        public override void Execute()
        {
            SceneManager.LoadScene("Menu");
        }
    }
}