﻿using Khadga.Fusion.Service;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Controller
{
    public class 
        GetPlayerCommand : Command
    {
        [Inject]
        public DataService DataService { get; set; }

        public override void Execute()
        {
            DataService.GetPlayerData();
        }
    }
}