﻿using Khadga.Shared;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using System;
using Khadga.Fusion.Model;

namespace Khadga.Fusion.Controller
{
    public class UpdateRechargeTimeCommand:Command
    {
        [Inject]
        public IPlayer Player { get; set; }

        public override void Execute()
        {
            SetTimer();
        }

        private void SetTimer()
        {
            Timer GameTimer = new Timer();
            GameTimer.Elapsed += new ElapsedEventHandler(UpdateTimer);
            GameTimer.Interval = Configuration.GAME_UPDATE_FREQUENCY;
            GameTimer.Start();
        }

        private void UpdateTimer(object sender, ElapsedEventArgs e)
        {
            if (Player.CanReadTimerValue && Player.RechargeTimer>0 && Player.NumberOfChargesLeft<4)
            {
                Player.RechargeTimer--;
            }
            else if (Player.RechargeTimer <= 0)
            {
                if (Player.NumberOfChargesLeft < Configuration.NUMBER_OF_RECHARGES)
                {
                    Player.NumberOfChargesLeft++;
                }
                Player.RechargeTimer = Configuration.EACH_RECHARGE_TIME;

            }

        }
    }
}


