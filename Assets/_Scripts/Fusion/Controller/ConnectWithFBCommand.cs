﻿using Khadga.Authentication;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion
{

    public class ConnectWithFBCommand : Command
    {

        [Inject]
        public IAuthenticationService AuthenticationService { get; set; }

        public override void Execute()
        {
            AuthenticationService.InitializeFBConnectivity();
        }
    }
}