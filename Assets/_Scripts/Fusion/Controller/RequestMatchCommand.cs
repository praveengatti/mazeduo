﻿using Khadga.GSBaas;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Controller
{
    public class RequestMatchCommand : Command
    {
        [Inject]
        public string ChallengeID { get; set; }
        [Inject]
        public GSRealTimeService GSRealTimeService { get; set; }

        public override void Execute()
        {
            GSRealTimeService.AcceptChallenge(ChallengeID);
        }
    }
}



