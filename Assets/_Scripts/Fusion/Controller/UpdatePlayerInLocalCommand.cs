﻿using Khadga.Fusion.Model;
using Khadga.Leaderboard;
using Khadga.Shared;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Controller
{
    public class UpdatePlayerInLocalCommand : Command
    {
        [Inject]
        public IPlayer Player { get; set; }

        [Inject]
        public Match Match { get; set; }

        [Inject]
        public ILeaderboardService LeaderboardService { get; set; }

        public override void Execute()
        {
            
            if (Match.IsMultiplayerMatch)
            {

                if (Player.MultiplayerBestTime == 0)
                {
                    Player.MultiplayerBestTime = Match.TimeTaken;
                    Player.BestPair = Player.Name + "/" + Match.Partner;
                }
                else if (Match.TimeTaken < Player.MultiplayerBestTime)
                {
                    Player.MultiplayerBestTime = Match.TimeTaken;
                    Player.BestPair = Player.Name + "/" + Match.Partner;
                }

                LeaderboardService.SubmitMultiplayerLeaderboard(Match.TimeTaken, Player.BestPair);
            }
            else
            {
                if (Player.BestTime == 0)
                {
                    Player.BestTime = Match.TimeTaken;
                }
                else if (Player.BestTime > Match.TimeTaken)
                {
                    Player.BestTime = Match.TimeTaken;
                }
            }

        }
    }
}