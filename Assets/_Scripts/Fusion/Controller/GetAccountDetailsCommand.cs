﻿using Khadga.Authentication;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion.Controller
{
    public class GetAccountDetailsCommand : Command
    {
        [Inject]
        public IAuthenticationService Authentication { get; set; }

        public override void Execute()
        {
            Authentication.GetAccoutDetails();
        }
    }
}