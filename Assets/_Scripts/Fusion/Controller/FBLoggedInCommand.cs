﻿using Khadga.Fusion.Model;
using Khadga.Shared;
using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion.Controller
{

    public class FBLoggedInCommand : Command
    {
        [Inject]
        public IPlayer Player { get; set; }

        public override void Execute()
        {
            Player.IsFBLoggedIn = true;
            Configuration.IsFBAvailable = true;
        }

    }
}