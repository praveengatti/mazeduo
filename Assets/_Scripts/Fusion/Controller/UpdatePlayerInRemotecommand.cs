﻿using Khadga.Fusion.Service;
using Khadga.Shared;
using Khadga.Utility.Time;
using strange.extensions.command.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Controller
{
    public class UpdatePlayerInRemoteCommand : Command
    {
        [Inject]
        public DataService DataService { get; set; }

        [Inject]
        public IPlayer Player { get; set; }

        public override void Execute()
        {
            Player.LastActivity = EpochUtil.GetEpochFromDateTime(DateTime.Now);
            DataService.UpdatePlayer(Player);
        }

    }
}