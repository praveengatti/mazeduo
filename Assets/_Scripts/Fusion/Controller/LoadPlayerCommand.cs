﻿using Khadga.Fusion.Model;
using Khadga.Shared;
using Khadga.Utility.Time;
using strange.extensions.command.impl;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Controller
{
    public class LoadPlayerCommand : Command
    {
        [Inject]
        public IPlayer Player { get; set; }
        [Inject]
        public StartMenuSignal StartMenuSignal { get; set; }
        [Inject]
        public UpdateTimerSignal UpdateTimerSignal { get; set; }
        [Inject]
        public ConnectWithFBSignal ConnectWithFBSignal { get; set; }

        public override void Execute()
        {
            Retain();
            injectionBinder.Bind<IPlayer>().ToValue(Player).ToSingleton().CrossContext();

            ConfigurePlayer();
            StartMenuSignal.Dispatch();
            UpdateTimerSignal.Dispatch();
            Release();
        }

        private void ConfigurePlayer()
        {
            Player.CanReadTimerValue = true;

            var secDiff = EpochUtil.GetTimeSpanBetweenEpochs( EpochUtil.GetEpochFromDateTime(DateTime.Now), Player.LastActivity).TotalSeconds + Player.RechargeTimer;


            if (secDiff < Configuration.EACH_RECHARGE_TIME )
            {
                Player.NumberOfChargesLeft = 0;
            }
            else if (secDiff < 2*Configuration.EACH_RECHARGE_TIME)
            {
                Player.NumberOfChargesLeft = 1;
            }
            else if (secDiff < 3*Configuration.EACH_RECHARGE_TIME )
            {
                Player.NumberOfChargesLeft = 2;
            }
            else if (secDiff < 4*Configuration.EACH_RECHARGE_TIME)
            {
                Player.NumberOfChargesLeft = 3;
            }
            else if (secDiff >= 4* Configuration.EACH_RECHARGE_TIME)
            {
                Player.NumberOfChargesLeft = 4;
                Player.RechargeTimer = Configuration.EACH_RECHARGE_TIME;
            }

        }

    }
}