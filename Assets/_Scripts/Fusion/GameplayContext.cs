﻿using Khadga.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.impl;
using Khadga.Fusion.Views;

namespace Khadga.Fusion
{
    public class GameplayContext : SignalContext
    {
        public GameplayContext(ContextView contextView) : base(contextView)
        {
        }

        protected override void mapBindings()
        {
            mediationBinder.Bind<GameplayView>().To<GameplayMediator>();

            mediationBinder.Bind<PlayerView>().To<PlayerMediator>();

            injectionBinder.Bind<GameEndSignal>().ToSingleton();

        }
    }
}