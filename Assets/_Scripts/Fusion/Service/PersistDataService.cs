﻿using Khadga.Shared;
using Khadga.Utility.LocalData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Assets.SimpleAndroidNotifications;
using Khadga.Fusion.Model;
using Khadga.Utility.Time;

namespace Khadga.Fusion
{
    public class PersistDataService
    {
        [Inject]
        public IPlayer Player { get; set; }

        [Inject]
        public UpdatePlayerInRemoteSignal UpdatePlayerInRemoteSignal { get; set; }


        public void Persist()
        {
            LocalDataUtil.SaveToLocal<IPlayer>(Player, "Player");
            UpdatePlayerToRemote();
            SetNotifications();
        }

        private void UpdatePlayerToRemote()
        {
            Player.LastActivity = EpochUtil.GetEpochFromDateTime(DateTime.Now);
        }

        private void SetNotifications()
        {
            NotificationManager.CancelAll();
            NotificationManager.SendWithAppIcon(TimeSpan.FromSeconds(15), "Maze Duo", "All charges are full, time to get aMazed", new Color(0, 0.6f, 1), NotificationIcon.Message);
        }
    }
}