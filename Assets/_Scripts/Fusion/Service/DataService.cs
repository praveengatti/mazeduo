﻿using Khadga.Fusion.Model;
using Khadga.Shared;
using System.Collections;
using System.Collections.Generic;
using Khadga.Utility;
using UnityEngine;
using Khadga.Utility.Time;
using System;
using Newtonsoft.Json;

namespace Khadga.Fusion.Service
{
    public class DataService
    {
        [Inject]
        public LoadPlayerSignal LoadPlayerSignal { get; set; }

        public void GetPlayerData()
        {
            IPlayer player;
               new GameSparks.Api.Requests.LogEventRequest().SetEventKey("GetPlayer").
                Send((response)=>
                {
                    if (response.HasErrors)
                    {

                    }
                    else
                    {
                        var res = response.ScriptData.GetGSData("Player");
                        Debug.Log(res.JSON.ToString());
                        player = new Player();
                        player.Name = res.GetString("Name");
                        player.NickName = res.GetString("NickName");
                        player.XP = (int)res.GetInt("XP");
                        player.IsFBLoggedIn = (bool)res.GetBoolean("IsFBLoggedIn");
                        player.Level = (int)res.GetInt("Level");
                        player.IsPremiumUser = (bool)res.GetBoolean("IsAPremiumUser");
                        player.BestTime = res.GetFloat("BestTime")??0;
                        player.NumberOfMagesSolved = (int)res.GetInt("NumberOfMagesSolved");
                        player.NumberOfChargesLeft = (int)res.GetInt("NumberOfChargesLeft");
                        player.RechargeTimer = (float)res.GetFloat("RechargeTimer");
                        if (res.GetLong("LastActivity") != null)
                        {
                            player.LastActivity = (long)res.GetLong("LastActivity");
                        }
                        else
                        {
                            player.LastActivity = 0;
                        }
                        LoadPlayerSignal.Dispatch(player);
                    }
                }
                );
        }

        public void UpdatePlayer(IPlayer player)
        {
            string jsonParam = "{\"NumberOfMagesSolved\":0,\"NumberOfChargesLeft\":3,\"BestTime\":0}";

            Debug.LogWarning("Update request :"+JsonConvert.SerializeObject((object) player));
            new GameSparks.Api.Requests.LogEventRequest().SetEventKey("UpdatePlayer")
                .SetEventAttribute("Player", JsonConvert.SerializeObject((object)player))
                .SetEventAttribute("BestScore", TimeFormatUtil.SecFloatToLong(player.BestTime))
                //.SetEventAttribute("BestPair", player.BestPair)
                //.SetEventAttribute("MultiplayerBestTime", TimeFormatUtil.SecFloatToLong(player.MultiplayerBestTime))
                .Send((response) =>
            {

            });
        }

    }
}





