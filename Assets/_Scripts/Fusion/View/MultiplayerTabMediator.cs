﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.Fusion.Model;
using Khadga.Shared;

namespace Khadga.Fusion.Views
{
    public class MultiplayerTabMediator : Mediator
    {
        [Inject]
        public MultiplayerTabView MultiplayerTabView { get; set; }
        [Inject]
        public ConnectWithFBSignal ConnectWithFBSignal { get; set; }
        [Inject]
        public FBLoggedInSignal FBLoggedInSignal { get; set; }
        [Inject]
        public FBFriendsListResponseSignal FBFriendsListResponseSignal { get; set; }
        [Inject]
        public ChallengeFriendSignal ChallengeFriendSignal { get; set; }
        [Inject]
        public ChallengeIssuedSignal ChallengeIssuedSignal { get; set; }
        [Inject]
        public ChallengeAcceptedSignal ChallengeAcceptedSignal { get; set; }
        [Inject]
        public IPlayer Player { get; set; }


        public override void OnRegister()
        {
            MultiplayerTabView.Initialize();
            MultiplayerTabView.FBConnectClickedSignal.AddListener(SendConnectToFBSignal);
            FBLoggedInSignal.AddListener(OnFBConnect);
            FBFriendsListResponseSignal.AddListener(SetFBFriends);
            ChallengeIssuedSignal.AddListener(ShowChallengePopup);
            MultiplayerTabView.ChallengeAcceptedSignal.AddListener(RequestStartMatch);
 

        }

        public override void OnRemove()
        {
            MultiplayerTabView.FBConnectClickedSignal.RemoveListener(SendConnectToFBSignal);
            FBLoggedInSignal.RemoveListener(OnFBConnect);
            FBFriendsListResponseSignal.RemoveListener(SetFBFriends);
            ChallengeIssuedSignal.RemoveListener(ShowChallengePopup);
            MultiplayerTabView.ChallengeAcceptedSignal.RemoveListener(RequestStartMatch);


            foreach (var friend in MultiplayerTabView.OnlineFriends)
            {
                friend.InviteClickedSignal.RemoveListener(InviteFriend);
            }

        }

        private void RequestStartMatch(string challengeId)
        {
            ChallengeAcceptedSignal.Dispatch(challengeId);
        }

        private void ShowChallengePopup(ChallengeDetails challengeDetails)
        {
            MultiplayerTabView.ShowChallengePopup(challengeDetails);
        }

        private void SetFBFriends(List<FBProfile> fbFriends)
        {
            MultiplayerTabView.SetOnlineFriends(fbFriends);
            foreach (var friend in MultiplayerTabView.OnlineFriends)
            {
                friend.Initialize();
                friend.InviteClickedSignal.AddListener(InviteFriend);
            }
        }

        private void InviteFriend(string fbId)
        {
            var fbIds = new List<string>();
            fbIds.Add(fbId);
            ChallengeFriendSignal.Dispatch(fbIds);
        }

        private void OnFBConnect(FBProfile fbProfile)
        {
            MultiplayerTabView.FBProfile = fbProfile;
            MultiplayerTabView.OnFBConnection();
        }

        private void SendConnectToFBSignal()
        {
            ConnectWithFBSignal.Dispatch();
        }


    }
}