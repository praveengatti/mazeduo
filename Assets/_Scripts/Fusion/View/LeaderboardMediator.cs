﻿using System;
using System.Collections.Generic;
using strange.extensions.mediation.impl;
using UnityEngine;
using Khadga.Shared;
using Khadga.Fusion;
using Khadga.Fusion.Model;

namespace Khadga.Leaderboard
{
    public class LeaderboardMediator : Mediator
    {
        [Inject]
        public LeaderboardView LeaderboardView { get; set; }

        [Inject]
        public LeaderboardResponseSignal LeaderboardResponseSignal { get; set; }

        [Inject]
        public MultiplayerLeaderboarResponseSignal MultiplayerLeaderboarResponseSignal { get; set; }

        [Inject]
        public RequestLeaderboardSignal RequestLeaderboardSignal { get; set; }

        [Inject]
        public IPlayer Player { get; set; }

        [Inject]
        public FBLoggedInSignal FBAuthenticatedSignal { get; set; }

        public override void OnRegister()
        {
            LeaderboardView.Initialize();
            LeaderboardResponseSignal.AddListener(OnLeaderboardResponse);
            MultiplayerLeaderboarResponseSignal.AddListener(OnMultiplayerLBResponse);
            FBAuthenticatedSignal.AddListener(OnFBConnect);
            if (Player.IsFBLoggedIn)
            {
                OnFBConnect(new FBProfile());
            }
            else
            {
                LeaderboardView.ShowBeforeLogin();
            }
        }

        public override void OnRemove()
        {
            LeaderboardResponseSignal.RemoveListener(OnLeaderboardResponse);
            MultiplayerLeaderboarResponseSignal.RemoveListener(OnMultiplayerLBResponse);

            FBAuthenticatedSignal.RemoveListener(OnFBConnect);

        }

        private void OnMultiplayerLBResponse(List<LeaderboardItem> obj)
        {
            LeaderboardView.UpdateMultiplayerLeaderboard(obj);
        }

        private void OnFBConnect(FBProfile fbp)
        {
            RequestLeaderboardSignal.Dispatch();
            LeaderboardView.ShowAfterLogin();
        }

        private void OnLeaderboardResponse(List<LeaderboardItem> leaderboardItemList)
        {
            LeaderboardView.UpdateLeaderboard(leaderboardItemList);
        }

    }
}