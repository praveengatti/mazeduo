﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.Shared;

namespace Khadga.Leaderboard
{
    public class LeaderboardView : UIView
    {
        public UIView BeforeLogin;
        public UIView AfterLogin;

        public LeaderboardItemView LeaderboardItemView;

        public UIView IndividualLeaderboard;
        public UIView MultiplayerLeaderboard;

        public Transform IndividualListParent;
        public Transform MultiplayerListParent;


        internal List<LeaderboardItemView> LeaderboardItemViewList;
        internal List<LeaderboardItemView> MultiplayerLeaderboardItemViewList;


        public void Initialize()
        {
            LeaderboardItemViewList = new List<LeaderboardItemView>();
            MultiplayerLeaderboardItemViewList = new List<LeaderboardItemView>();
        }

        public void UpdateLeaderboard(List<LeaderboardItem> leaderboardItemsList)
        {
            FlushCurrentLeaderboard(LeaderboardItemViewList);
            foreach (var item in leaderboardItemsList)
            {
                var lbItem = GameObject.Instantiate<LeaderboardItemView>(LeaderboardItemView);
                lbItem.transform.SetParent(IndividualListParent, false);
                lbItem.gameObject.SetActive(true);
                lbItem.Rank.text = item.Rank.ToString();
                lbItem.Name.text = item.Name;
                lbItem.Time.text = item.DecisionAttribute1;
                LeaderboardItemViewList.Add(lbItem);
            }
            OnIndividualLeaderboardClick();
        }

        public void UpdateMultiplayerLeaderboard(List<LeaderboardItem> leaderboardItemsList)
        {
            FlushCurrentLeaderboard(MultiplayerLeaderboardItemViewList);
            foreach (var item in leaderboardItemsList)
            {
                var lbItem = GameObject.Instantiate<LeaderboardItemView>(LeaderboardItemView);
                lbItem.transform.SetParent(MultiplayerListParent, false);
                lbItem.gameObject.SetActive(true);
                lbItem.Rank.text = item.Rank.ToString();
                lbItem.Name.text = item.Name;
                lbItem.Time.text = item.DecisionAttribute1;
                MultiplayerLeaderboardItemViewList.Add(lbItem);
            }
        }

        private void FlushCurrentLeaderboard(List<LeaderboardItemView> LeaderboardItemViewList)
        {
            foreach(var item in LeaderboardItemViewList)
            {
                Destroy(item.gameObject);
            }

            LeaderboardItemViewList.Clear();
        }

        internal void ShowAfterLogin()
        {
            BeforeLogin.Hide();
            AfterLogin.Show();
        }

        internal void ShowBeforeLogin()
        {
            AfterLogin.Hide();
            BeforeLogin.Show();
        }

        public void OnIndividualLeaderboardClick()
        {
            MultiplayerLeaderboard.Hide();
            IndividualLeaderboard.Show();
        }

        public void OnMultiplayerLeaderboardClick()
        {
            IndividualLeaderboard.Hide();
            MultiplayerLeaderboard.Show();
        }
    }
}