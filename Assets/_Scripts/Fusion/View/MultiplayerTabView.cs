﻿using Khadga.Fusion.Model;
using Khadga.Shared;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.Fusion.Views
{
    public class MultiplayerTabView : View
    {
        public UIView BeforeFBLogin;
        public UIView AfterFBLogin;

        public RawImage ProfilePic;
        public Text PlayerName;

        internal FBProfile FBProfile;

        public Signal FBConnectClickedSignal { get; set; }

        public RectTransform OnlineFriendsContainer;
        public FriendItemView OnlineFriendItemView;
        internal List<FriendItemView> OnlineFriends;

        public GameObject ChallengePopup;
        public Text ChallengerName;
        internal string RecentChallengeId;

        internal Signal<string> ChallengeAcceptedSignal { get; set; }

        public void Initialize()
        {
            FBConnectClickedSignal = new Signal();
            ChallengeAcceptedSignal = new Signal<string>();
            SetBeforeFBLoginTab();
            OnlineFriends = new List<FriendItemView>();
        }

        public void OnFBConnection()
        {
            PlayerName.text = FBProfile.UserName;
            StartCoroutine(SetProfilePic(FBProfile));
            ProfilePic.texture = FBProfile.ProfilePicTexture;
            SetAfterFBLoginTab();
        }

        private IEnumerator SetProfilePic(FBProfile fbProfile)
        {
            WWW www = new WWW("http://graph.facebook.com/" + fbProfile.FacebookID + "/picture?width=210&height=210");
            yield return www;
            www.LoadImageIntoTexture(fbProfile.ProfilePicTexture);
        }

        #region BeforeLogin

        public void OnFBLoginClick()
        {
            FBConnectClickedSignal.Dispatch();
        }

        #endregion

        internal void SetBeforeFBLoginTab()
        {
            AfterFBLogin.Hide();
            BeforeFBLogin.Show();
        }

        internal void SetAfterFBLoginTab()
        {
            BeforeFBLogin.Hide();
            AfterFBLogin.Show();
        }

        public void OnChallengeAcceptClick()
        {
            ChallengeAcceptedSignal.Dispatch(RecentChallengeId);
            ChallengePopup.gameObject.SetActive(false);
        }

        internal void SetOnlineFriends(List<FBProfile> fbFriends)
        {
            foreach (var friend in fbFriends)
            {
                StartCoroutine(SetProfilePic(friend));

                var onlineFriend = GameObject.Instantiate<FriendItemView>(OnlineFriendItemView);
                onlineFriend.transform.SetParent(OnlineFriendsContainer, false);
                onlineFriend.FacebookID = friend.FacebookID;
                onlineFriend.GSID = friend.GSID;
                onlineFriend.gameObject.SetActive(true);
                onlineFriend.ProfilePic.texture = friend.ProfilePicTexture;
                onlineFriend.Name.text = friend.UserName;
                OnlineFriends.Add(onlineFriend);
            }
        }

        internal void ShowChallengePopup(ChallengeDetails cd)
        {
            ChallengePopup.gameObject.SetActive(true);
            ChallengerName.text = cd.UserName;
            RecentChallengeId = cd.ChallengeID;
        }

    }
}