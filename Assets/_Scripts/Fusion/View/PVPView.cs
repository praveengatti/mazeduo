﻿using GameSparks.RT;
using Khadga.Utility;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion.Views
{
    public class PVPView : GameplayView
    {
        public PVPPlayerView RedBall;
        public PVPPlayerView BlueBall;

        [HideInInspector]
        public PVPPlayerView Opponent;

        public GameObject wall;
        public GameObject GenerationPlane; // this is the parent of all the walls going to be generated
        private List<Vector3> pathMazes = new List<Vector3>();

        public new void Initialize()
        {
            base.Initialize();
        }

        internal void GenerateMaze(int[,] maze)
        {

            GameObject ptype = null;

            for (int i = 0; i <= maze.GetUpperBound(0); i++)
            {
                for (int j = 0; j <= maze.GetUpperBound(1); j++)
                {
                    if (maze[i, j] == 1)
                    {
                        ptype = wall;
                        var x = (GameObject)Instantiate(wall, new Vector3(i * ptype.transform.localScale.x, 1, j * ptype.transform.localScale.z), Quaternion.identity);

                        x.transform.SetParent(GenerationPlane.transform, true);
                    }
                    else if (maze[i, j] == 0)
                    {
                        pathMazes.Add(new Vector3(i, j, 0));
                    }

                }
            }
        }

        public void MoveOpponent(Vector3 vector3)
        {
            Opponent.MoveTo(vector3);
        }

    }
}