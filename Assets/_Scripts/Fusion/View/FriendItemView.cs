﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Khadga.Fusion.Views
{
    public class FriendItemView : View
    {
        public Text Name;
        public RawImage ProfilePic;
        public Text Best;

        [HideInInspector]
        public string FacebookID;
        [HideInInspector]
        public string GSID;
        internal Signal<string> InviteClickedSignal;
        internal Signal JoinClickedSignal;

        public void Initialize()
        {
            InviteClickedSignal = new Signal<string>();
            JoinClickedSignal = new Signal();
        }

        public void OnInviteClick()
        {
            InviteClickedSignal.Dispatch(GSID);
        }

        public void OnJoinClick()
        {
            JoinClickedSignal.Dispatch();
        }
    }
}




