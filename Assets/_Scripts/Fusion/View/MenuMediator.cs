﻿using Khadga.Shared;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Khadga.Fusion.Views
{
    public class MenuMediator : Mediator
    {
        [Inject]
        public MenuView MenuView { get; set; }

        [Inject]
        public GetAccountDetailsSignal GetAccountDetailsSignal { get; set; }

        [Inject]
        public IPlayer Player { get; set; }

        public override void OnRegister()
        {
            MenuView.Initialize();
            MenuView.Player = Player;
            MenuView.SetCharges(4, Player.NumberOfChargesLeft);
            MenuView.ClickedSignal.AddListener(OnClick);

            if (Player.IsFBLoggedIn == true)
            {
                GetAccountDetailsSignal.Dispatch();
            }
        }

        public override void OnRemove()
        {
            MenuView.ClickedSignal.RemoveListener(OnClick);

        }

        private void OnClick(MenuView.ClickType buttonType)
        {
            GetComponent<AudioSource>().PlayOneShot(MenuView.ButtonClickAudio);
            switch(buttonType)
            {
                case MenuView.ClickType.Play:
                    {
                        

                        break;
                    }
            }
        }
    }
}