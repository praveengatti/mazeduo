﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Khadga.Fusion.Views
{
    public class PVPPlayerMediator : Mediator
    {
        [Inject]
        public PVPPlayerView PVPPlayerView { get; set; }
        [Inject]
        public PVPGameEndSignal PVPGameEndSignal { get; set; }

        public override void OnRegister()
        {
            PVPPlayerView.Initialize();
            PVPPlayerView.BallsCollidedSignal.AddListener(OnBallsCollide);
        }

        public override void OnRemove()
        {
            PVPPlayerView.BallsCollidedSignal.RemoveListener(OnBallsCollide);

        }

        private void OnBallsCollide()
        {
            PVPGameEndSignal.Dispatch();
        }
    }
}