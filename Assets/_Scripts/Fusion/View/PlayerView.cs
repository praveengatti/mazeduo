﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;

namespace Khadga.Fusion.Views
{
    public class PlayerView : View
    {

        public float speed;
        private Rigidbody rb;
        bool IsInitialized = false;
        internal Signal BallsCollidedSignal { get; set; }
        Vector3 TouchPosition;

        internal void Initialize()
        {
            rb = this.gameObject.GetComponent<Rigidbody>();
            BallsCollidedSignal = new Signal();
            IsInitialized = true;
        }

        void FixedUpdate()
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");
            float moveHorizontalPhone = Input.acceleration.x;
            float moveVerticalPhone = Input.acceleration.y;

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            Vector3 movementPhone = new Vector3(moveHorizontalPhone, 0.0f, moveVerticalPhone);
            if (IsInitialized)
            {
                rb.AddForce(movement * speed);
                rb.AddForce(movementPhone * speed);
            }

            if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
            {
                TouchPosition = Input.GetTouch(0).position;
                Vector3 realWorldPos = Camera.main.ScreenToWorldPoint(TouchPosition);
                GameObject.Instantiate(Resources.Load("Hit"), realWorldPos, this.transform.rotation);
 
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Player2" && this.gameObject.tag == "Player")
            {
                BallsCollidedSignal.Dispatch();
            }
            else if (collision.gameObject.tag == "Collectible")
            {
                Destroy(collision.gameObject);
                GameObject.Instantiate (Resources.Load ("CE"), this.transform.position, this.transform.rotation);
            }
        }

    }
}