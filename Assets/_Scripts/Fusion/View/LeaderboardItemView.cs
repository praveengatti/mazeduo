﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.Leaderboard
{
    public class LeaderboardItemView : View
    {
        public Text Rank;
        public Text Name;
        public Text Time;
    }
}