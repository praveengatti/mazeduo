﻿using GameSparks.RT;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Khadga.Fusion.Views
{
    public class PVPPlayerView : View
    {
        [HideInInspector]
        public int PeerID;

        public float speed;
        private Rigidbody rb;
        [HideInInspector]
        public bool IsUser = false;
        internal Signal BallsCollidedSignal { get; set; }
        Vector3 PreviousPosition;

        internal void Initialize()
        {
            rb = this.gameObject.GetComponent<Rigidbody>();
            BallsCollidedSignal = new Signal();
            PreviousPosition = Vector3.zero;
        }

        void FixedUpdate()
        {
            if (IsUser)
            {
                float moveHorizontal = Input.GetAxis("Horizontal");
                float moveVertical = Input.GetAxis("Vertical");

                float moveHorizontalPhone = Input.acceleration.x;
                float moveVerticalPhone = Input.acceleration.y;

                Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
                Vector3 movementPhone = new Vector3(moveHorizontalPhone, 0.0f, moveVerticalPhone);

                rb.AddForce(movement * speed);
                rb.AddForce(movementPhone * speed);
                if (this.transform.position != PreviousPosition)
                {
                    SendPosition();
                    PreviousPosition = this.transform.position;
                }
            }
        }

        public void MoveTo(Vector3 targetPosition)
        {
            if (!this.IsUser && this != null)
            {
                this.transform.position = targetPosition;
            }
        }

        public void SendPosition()
        {
            using (RTData data = RTData.Get())
            {  // we put a using statement here so that we can dispose of the RTData objects once the packet is sent
                data.SetVector3(2, this.gameObject.transform.position);
                GSRTManager.Instance().GetRTSession().SendData(2, GameSparks.RT.GameSparksRT.DeliveryIntent.UNRELIABLE_SEQUENCED, data);// send the data
            }
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (collision.gameObject.tag == "Player2" || collision.gameObject.tag == "Player")
            {
                BallsCollidedSignal.Dispatch();
            }
            
            else if (collision.gameObject.tag == "Object" && collision.impulse.magnitude>7)
            {
                GameObject.Instantiate(Resources.Load("Hit"), this.transform.position, this.transform.rotation);
            }
        }


    }
}