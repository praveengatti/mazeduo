﻿using Khadga.Fusion.Model;
using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.Utility.Time;
using Khadga.Shared;
using GameSparks.RT;

namespace Khadga.Fusion.Views
{
    public class PVPMediator : Mediator
    {
        [Inject]
        public PVPView PVPView { get; set; }

        [Inject]
        public IPlayer Player { get; set; }

        [Inject]
        public ChallengeDetails ChallengeDetails { get; set; }

        [Inject]
        public PVPGameEndSignal PVPGameEndSignal { get; set; }

        [Inject]
        public OpponentRTSignal OpponentRTSignal { get; set; }

        [Inject]
        public UpdatePlayerInLocalSignal UpdatePlayerInLocalSignal { get; set; }

        [Inject]
        public UpdatePlayerInRemoteSignal UpdatePlayerInRemoteSignal { get; set; }

        public override void OnRegister()
        {
            PVPView.Initialize();
            PVPGameEndSignal.AddListener(OnGameEnd);
            OpponentRTSignal.AddListener(ExecuteOpponentRTPacket);
            SetPVPView();
            PVPView.GenerateMaze(ChallengeDetails.ChallengeMaze);
        }

        public override void OnRemove()
        {
            PVPGameEndSignal.RemoveListener(OnGameEnd);
            OpponentRTSignal.RemoveListener(ExecuteOpponentRTPacket);
        }

        private void MoveOpponent(Vector3 obj)
        {
            PVPView.MoveOpponent(obj);
        }

        private void SetPVPView()
        {
            if (ChallengeDetails.UserPeerID == 1)
            {
                PVPView.RedBall.IsUser = true;
                PVPView.BlueBall.IsUser = false;
                PVPView.Opponent = PVPView.BlueBall;
                PVPView.RedBall.PeerID = 1;
                PVPView.BlueBall.PeerID = 2;
            }
            else
            {
                PVPView.RedBall.IsUser = false;
                PVPView.BlueBall.IsUser = true;
                PVPView.Opponent = PVPView.RedBall;
                PVPView.RedBall.PeerID = 2;
                PVPView.BlueBall.PeerID = 1;
            }
        }

        private void OnGameEnd()
        {
            Time.timeScale = 0;
            var timeTaken = Time.time - PVPView.StartTime;
            
            PVPView.CurrentTime.text = PVPView.Timer.text;
            PVPView.CurrentTime.text = TimeFormatUtil.SecondToHHMMSSmm(timeTaken);
            PVPView.GameEndPopup.SetActive(true);

            Match m = new Match();
            m.IsMultiplayerMatch = true;
            m.TimeTaken = timeTaken;
            m.Partner = ChallengeDetails.OpponentName;
            if (ChallengeDetails.UserPeerID == 1)
            {
                UpdatePlayerInLocalSignal.Dispatch(m);
            }
            UpdatePlayerInRemoteSignal.Dispatch(Player);
        }

        public void ExecuteOpponentRTPacket(RTPacket _packet)
        {
            switch (_packet.OpCode)
            {
                case 1:
                    {
                        break;
                    }
                case 2:
                    {
                        MoveOpponent((Vector3)_packet.Data.GetVector3(2));
                        break;
                    }
                case 3:
                    {
                        break;
                    }
                case 4:
                    {
                        break;
                    }
            }
        }
    }
}