﻿using Khadga.Fusion.Model;
using Khadga.Shared;
using Khadga.Utility.Time;
using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Khadga.Fusion.Views
{
    public class MenuView : View
    {
        public Image[] Charges;
        public Animator LeaderboardAnimator;
        public Animator MultiplayerAnimator;
        public Animator ShopAnimator;
        Animator ActiveTabAnimator;
        public AudioClip ButtonClickAudio;
        public AudioClip ChargeDownAudio;
        public GameObject LoadingPanel;
        internal Signal<ClickType> ClickedSignal { get; set; }
        bool InProgress;
        public GameObject NoChargesPopup;
        public Text WaitingTimeText;
        internal IPlayer Player;

        internal enum ClickType
        {
            Play,
            Multiplayer,
            Shop,
            Leaderboard
        }

        public void Initialize()
        {
            ClickedSignal = new Signal<ClickType>();
            OnLeaderboardClick();
            SetTimer();
        }

        public void SetCharges(int totalCharges, int chargesLeft)
        {
            for (int iter = 0; iter < totalCharges; iter++)
            {
                if (iter < chargesLeft)
                {
                    Charges[iter].sprite = Resources.Load<Sprite>("ChargeFilled");
                }
                else
                {
                    Charges[iter].sprite = Resources.Load<Sprite>("ChargeEmpty");
                }
            }
        }


        public void OnPlayClick()
        {
            GetComponent<AudioSource>().PlayOneShot(ChargeDownAudio);
            if (Player.NumberOfChargesLeft > 0)
            {
                Charges[Player.NumberOfChargesLeft - 1].gameObject.GetComponent<Animator>().SetTrigger("ChargeDown");
                Player.NumberOfChargesLeft--;
                StartCoroutine(DelayGamePlay());
            }
            else
            {
                NoChargesPopup.SetActive(true);
            }
            ClickedSignal.Dispatch(ClickType.Play);
        }


        public void OnMultiplayerClick()
        {
            ClickedSignal.Dispatch(ClickType.Multiplayer);
            ViewTab(MultiplayerAnimator);

        }

        public void OnLeaderboardClick()
        {
            ClickedSignal.Dispatch(ClickType.Leaderboard);
            ViewTab(LeaderboardAnimator);
        }

        public void OnShopClick()
        {
            ClickedSignal.Dispatch(ClickType.Shop);
            ViewTab(ShopAnimator);
        }

        public void ViewTab(Animator tabAnimator)
        {
            if (ActiveTabAnimator != tabAnimator)
            {
                HideActiveTab(); tabAnimator.SetTrigger("SlideIn");
                ActiveTabAnimator = tabAnimator;
               
            }
            else
            {
                tabAnimator.SetTrigger("Focus");
            }
        }

        public void HideActiveTab()
        {
            if (ActiveTabAnimator != null)
                ActiveTabAnimator.SetTrigger("SlideOut");
        }


        // delay gameplay for charge down animation

        IEnumerator DelayGamePlay()
        {
            yield return new WaitForSeconds(0.5f);
            var async = SceneManager.LoadSceneAsync("Gameplay");

            StartCoroutine(ShowLoading(async));
        }

        private IEnumerator ShowLoading(AsyncOperation asyncOperation)
        {
            InProgress = true;

            LoadingPanel.SetActive(true);

            while (asyncOperation.progress < 0.98f)
            {
                yield return null;
            }

            InProgress = false;
            LoadingPanel.SetActive(false);
        }

        internal void SetTimer()
        {
            InvokeRepeating("UpdateTimerText", 0.1f, 1f);
        }

        private void UpdateTimerText()
        {
            WaitingTimeText.text = TimeFormatUtil.SecondToHHMMSSmm(Player.RechargeTimer);
        }

        public void CloseNoChargePopup()
        {
            NoChargesPopup.gameObject.SetActive(false);
        }

    }
}






