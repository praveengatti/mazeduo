﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Khadga.Utility.Time;
using UnityEngine.SceneManagement;

namespace Khadga.Fusion.Views
{
    public class GameplayView : View
    {
        public Text Timer;
        internal float StartTime;

        public GameObject GameEndPopup;
        public GameObject LoadingPopup;
        public Text CurrentTime;
        public Text BestTime;


        public void Initialize()
        {
            SetTimer();
            InstantiateGameObjects();
        }


        private void InstantiateGameObjects()
        {
            //GameObject Ball1 = (GameObject)GameObject.Instantiate(Resources.Load("Player"));
            //GameObject Ball2 = (GameObject)GameObject.Instantiate(Resources.Load("Player2"));
        }

        private void SetTimer()
        {
            StartTime = Time.time;
        }

        private void UpdateTimer()
        {
            Timer.text = TimeFormatUtil.SecondToHHMMSSmm(Time.time - StartTime);
        }

        

        private void Update()
        {
            UpdateTimer();
        }

        public void OnMenuClick()
        {
            LoadingPopup.SetActive(true);
            SceneManager.LoadSceneAsync("Menu");
            Time.timeScale = 1;
        }

        public void SpawnCollectibleAtRandomPosition()
        {
            GameObject collectible = (GameObject)GameObject.Instantiate(Resources.Load("Collectible"));

        }
    }
}