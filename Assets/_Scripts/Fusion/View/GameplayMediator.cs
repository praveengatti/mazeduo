﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Khadga.Shared;
using Khadga.Fusion.Model;
using Khadga.Utility.Time;

namespace Khadga.Fusion.Views
{

    public class GameplayMediator : Mediator
    {
        [Inject]
        public GameplayView GameplayView { get; set; }

        [Inject]
        public GameEndSignal GameEndSignal { get; set; }

        [Inject]
        public IPlayer Player { get; set; }

        [Inject]
        public UpdatePlayerInRemoteSignal UpdatePlayerInRemoteSignal { get; set; }

        [Inject]
        public UpdatePlayerInLocalSignal UpdatePlayerInLocalSignal { get; set; }

        public override void OnRegister()
        {
            GameplayView.Initialize();
            GameEndSignal.AddListener(OnGameEnd);
        }

        public override void OnRemove()
        {
            GameEndSignal.RemoveListener(OnGameEnd);

        }

        private void OnGameEnd()
        {
            Time.timeScale = 0;
            var timeTaken = Time.time - GameplayView.StartTime;


            GameplayView.CurrentTime.text = GameplayView.Timer.text;
            GameplayView.BestTime.text = TimeFormatUtil.SecondToHHMMSSmm(Player.BestTime);
            GameplayView.CurrentTime.text = TimeFormatUtil.SecondToHHMMSSmm( timeTaken);
            GameplayView.GameEndPopup.SetActive(true);

            Match m = new Match();
            m.TimeTaken = timeTaken;
            UpdatePlayerInLocalSignal.Dispatch(m);
            UpdatePlayerInRemoteSignal.Dispatch(Player);
        }
    }
}