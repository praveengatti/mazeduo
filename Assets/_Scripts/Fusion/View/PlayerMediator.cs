﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Khadga.Fusion.Views
{
    public class PlayerMediator : Mediator
    {
        [Inject]
        public PlayerView PlayerView { get; set; }

        [Inject]
        public GameEndSignal GameEndSignal { get; set; }

        public override void OnRegister()
        {
            PlayerView.Initialize();
            PlayerView.BallsCollidedSignal.AddListener(OnBallsCollide);
        }

        public override void OnRemove()
        {
            PlayerView.BallsCollidedSignal.RemoveListener(OnBallsCollide);

        }

        private void OnBallsCollide()
        {
            GameEndSignal.Dispatch();
        }
    }
}