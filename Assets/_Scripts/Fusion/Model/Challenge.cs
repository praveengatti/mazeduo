﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion.Model
{
    public class ChallengeDetails
    {
        public string ChallengeID;
        public int[,] ChallengeMaze;

        public string OpponentName;
        public string OpponentFacebookID;
        public Texture2D OpponentProfilePicTexture;
        public float OpponentBest;
        public int OpponnetPeerID;
        public string OpponentAccessToken;

        public string UserName;
        public string UserFacebookID;
        public Texture2D UserProfilePicTexture;
        public float UserBest;
        public int UserPeerID;
        public string UserAccessToken;


        public ChallengeDetails()
        {
            OpponentProfilePicTexture = new Texture2D(25,25);
            UserProfilePicTexture = new Texture2D(25,25);
        }
    }
}