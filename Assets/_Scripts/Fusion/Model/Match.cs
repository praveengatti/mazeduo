﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Model
{
    public class Match
    {
        public float TimeTaken;
        public bool IsMultiplayerMatch;
        public string Partner;
    }
}