﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion.Model
{
    public class FBProfile
    {
        public string UserName;
        public string FacebookID;
        public Texture2D ProfilePicTexture;
        public string GSID;
        public float Best;

        public FBProfile()
        {
            ProfilePicTexture = new Texture2D(25,25);
        }

    }
}



