﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion.Model
{
    public static class Configuration
    {
        public const float EACH_RECHARGE_TIME = 15 * 60;

        public const int NUMBER_OF_RECHARGES = 4;

        public const float GAME_UPDATE_FREQUENCY = 1000f; // milli seconds

        public const int MazeWidth = 35;

        public const int MazeHeight = 15;

        public static bool IsFBAvailable;

    }
}
