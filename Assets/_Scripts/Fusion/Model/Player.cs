﻿using Khadga.Shared;
using System;
namespace Khadga.Fusion.Model
{
    public class Player : IPlayer
    {
        public string Name { get; set; }
        public string NickName { get; set; }
        public bool IsFBLoggedIn { get; set; }
        public bool CanReadTimerValue { get; set; }
        public float RechargeTimer { get; set; }
        public int XP { get; set; }
        public int Level { get; set; }
        public bool IsPremiumUser { get; set; }
        public int Wins { get; set; }
        public int Losses { get; set; }
        public int Draws { get; set; }
        public int WinStreak { get; set; }
        public int LoseStreak { get; set; }
        public int BestWinStreak { get; set; }
        public float BestTime { get; set; }
        public int NumberOfMagesSolved { get; set; }
        public int NumberOfChargesLeft { get; set; }
        public long LastActivity { get; set; }

        public float MultiplayerBestTime { get; set; }
        public string BestPair { get; set; }

        public Player()
        {
        }

        public Player(string name, string nickName, int chargesLeft, int xp, bool isPremiumUser,
            float bestTime,int numberOfMagesSolved, int numberOfChargesLeft, long lastActivity, float rechargeTimer, bool isFBLoggedIn)
        {
            this.Name = name;
            this.NickName = nickName;
            this.XP = xp;
            this.IsFBLoggedIn = isFBLoggedIn;
            this.IsPremiumUser = IsPremiumUser;
            this.BestTime = bestTime;
            this.NumberOfMagesSolved = numberOfMagesSolved;
            this.NumberOfChargesLeft = numberOfChargesLeft;
            this.RechargeTimer = rechargeTimer;
            this.LastActivity = lastActivity;
        }
    }
}