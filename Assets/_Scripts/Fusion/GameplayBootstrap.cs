﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Fusion
{
    public class GameplayBootstrap : ContextView
    {
        public void Awake()
        {
            context = new GameplayContext(this);
        }
    }
}