﻿using GameSparks.RT;
using Khadga.Fusion.Model;
using Khadga.Leaderboard;
using Khadga.Shared;
using strange.extensions.signal.impl;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Fusion
{
    public class StartIndexSignal: Signal { }

    public class StartMenuSignal : Signal { }

    public class LoadPlayerSignal : Signal<IPlayer> { }

    public class GetPlayerSignal : Signal { }

    public class GameEndSignal : Signal { }

    public class PVPGameEndSignal : Signal { }

    public class UpdatePlayerInRemoteSignal :Signal<IPlayer> { }

    public class UpdatePlayerInLocalSignal : Signal<Match> { }

    public class ConnectWithFBSignal :Signal { }

    public class FBLoggedInSignal :Signal<FBProfile> { }

    public class GetAccountDetailsSignal : Signal { }

    public class SaveToLocalSignal :Signal { }

    public class UpdateTimerSignal :Signal { }

    public class FBAuthenticatedSignal :Signal { }

    public class RequestLeaderboardSignal :Signal { }

    public class SampleSignal : Signal { }

    public class LeaderboardResponseSignal :Signal<List<LeaderboardItem>> { }

    public class MultiplayerLeaderboarResponseSignal: Signal<List<LeaderboardItem>> { }

    public class FBFriendsListResponseSignal : Signal<List<FBProfile>> { }

    public class ChallengeIssuedSignal :Signal<ChallengeDetails> { }

    public class ChallengeStartedSignal : Signal { }

    public class ChallengeFriendSignal : Signal<List<string>> { }

    public class ChallengeAcceptedSignal : Signal<string> { }

    public class OpponentRTSignal : Signal<RTPacket> { }

}











