﻿using Khadga.Shared;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using strange.extensions.context.impl;
using Khadga.Fusion.Views;
using Khadga.Leaderboard;
using Khadga.Fusion;

namespace Khadga.Menu
{
    public class MenuContext : SignalContext
    {
        public MenuContext(ContextView contextView) : base(contextView)
        {
            
        }

        protected override void mapBindings()
        {
            mediationBinder.Bind<MenuView>().To<MenuMediator>();
            mediationBinder.Bind<MultiplayerTabView>().To<MultiplayerTabMediator>();
            mediationBinder.Bind<LeaderboardView>().To<LeaderboardMediator>();


        }
    }
}