﻿using strange.extensions.context.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Menu
{
    public class MenuBootstrap : ContextView
    {
        private void Awake()
        {
            context = new MenuContext(this);
        }
    }
}