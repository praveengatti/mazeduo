﻿
using Golive.InAppPurchases.Model;
using strange.extensions.signal.impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Golive.InAppPurchases.Signals
{
    public class PurchaseInAppItemSignal : Signal<InAppItem> { }

    public class PurchaseSuccessfulSignal : Signal<Purchase> { };

    public class PurchaseFailedSignal : Signal<FailedPurchaseDetails> { };

}
