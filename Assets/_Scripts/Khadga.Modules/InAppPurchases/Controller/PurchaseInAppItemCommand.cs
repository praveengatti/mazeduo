﻿using Golive.InAppPurchases.Model;
using strange.extensions.command.impl;

namespace Golive.InAppPurchases.Commands
{

    public class PurchaseInAppItemCommand: Command
    {
        [Inject]
        public InAppItem ShopItem { get; set; }


        [Inject]
        public IShopService ShopService { get; set; }

        public override void Execute()
        {
            ShopService.Purchase(ShopItem);
        }
    }
}
