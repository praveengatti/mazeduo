﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Golive.InAppPurchases.Model;

namespace Golive.InAppPurchases
{
    public interface IShopService
    {
        void Purchase(InAppItem productToPurchase);
    }
}
