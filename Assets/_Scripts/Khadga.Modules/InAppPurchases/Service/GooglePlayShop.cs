﻿using UnityEngine;
using System.Collections;
using OnePF;
using strange.extensions.signal.impl;
using System;
using Golive.InAppPurchases.Model;
using System.Collections.Generic;

namespace Golive.InAppPurchases
{
    //TODO: VK, we will revist and refactor this to use InAppPurchases as a module in all the projects latter.

    public class GooglePlayShop : ShopService
    {       

        protected override string StoreKey
        {
            get
            {
                return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAqg9T/YqHvg1K5qJ7EFHR8U4G86Itr8f3uxvdJIJGNOLA2Ly6aU5kNzrYHlJUbTVnVJrCBk+jxJ9sBb9L2WzDEByuELEYplP1y3upFfQNp6WfWCQN5M/cUFjCJC/36KOvyxawEDID6TCsGdWQIXNAhP4QC1rHXBRkhsA3pbD+P/ANO5wnW61VxzV1a4h7JY53NTIzeyRMMgUwW1Nsf3+iPNval1sdIVz7+hgatf85qJ3Z6qvUSrCcew7V3ox8BoRxN5gYe7FL93Us/yXRSJvHTzViTKpKw9mtjebtEdc1pt8M/1BikOJBxZk5bYuiwlnmdmxJTE5E8tEO1fu75REpyQIDAQAB";
            }
        }

        protected override string Store
        {
            get
            {
                return OpenIAB_Android.STORE_GOOGLE;
            }

        }

    }

}
