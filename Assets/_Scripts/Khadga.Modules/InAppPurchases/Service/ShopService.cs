﻿using Golive.InAppPurchases.Model;
using Golive.InAppPurchases.Signals;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

//TODO: Refactor so that logger interface is injected, its implementation can wrap the Unity's Debug, etc 


namespace Golive.InAppPurchases
{
    //OpenIABEventManager has to be preserved as a singleton bu adding in the main scene
    public abstract class ShopService : IShopService
    {
        private bool _processingPayment = false;

        protected abstract string Store { get; }
        protected abstract string StoreKey { get; }

        [Inject]
        public InAppItems ShopItems { get; set; }

        private bool ISBillingSupported { get; set; }

        [Inject]
        public PurchaseSuccessfulSignal PurchaseSuccessfulSignal { get; set; }

        [Inject]
        public PurchaseFailedSignal PurchaseFailedSignal { get; set; }

        private OpenIABEventManager EventManager { get; set; }

        public ShopService()
        {
            ISBillingSupported = true;
        }

        private void MapSKUs()
        {
            foreach (string itemSKU in ShopItems.Keys)
            {
                OnePF.OpenIAB.mapSku(itemSKU, Store, itemSKU);
            }
        }

        [PostConstruct]
        public void Setup()
        {
            if (EventManager == null)
            {
                // Avoid duplication
                EventManager = GameObject.FindObjectOfType<OpenIABEventManager>();

                if (EventManager == null)
                    EventManager = new GameObject(typeof(OpenIABEventManager).ToString()).AddComponent<OpenIABEventManager>();
            }
            

            OpenIABEventManager.billingSupportedEvent += OnBillingSupported;
            OpenIABEventManager.billingNotSupportedEvent += OnBillingNotSupported;
            OpenIABEventManager.queryInventorySucceededEvent += OnQueryInventorySucceeded;
            OpenIABEventManager.queryInventoryFailedEvent += OnQueryInventoryFailed;
            OpenIABEventManager.purchaseSucceededEvent += OnPurchaseSucceded;
            OpenIABEventManager.purchaseFailedEvent += OnPurchaseFailed;
            OpenIABEventManager.consumePurchaseSucceededEvent += OnConsumePurchaseSucceeded;
            OpenIABEventManager.consumePurchaseFailedEvent += OnConsumePurchaseFailed;
            OpenIABEventManager.transactionRestoredEvent += OnTransactionRestored;
            OpenIABEventManager.restoreSucceededEvent += OnRestoreSucceeded;
            OpenIABEventManager.restoreFailedEvent += OnRestoreFailed;

            MapSKUs();

            OnePF.Options options = new OnePF.Options();
            options.storeKeys.Add(Store, StoreKey);
            OnePF.OpenIAB.init(options);
        }

        public void Purchase(InAppItem ShopItem)
        {
            if (ISBillingSupported)
            {
                if (ShopItems.ContainsKey(ShopItem.Sku) 
                    && ShopItems[ShopItem.Sku] == ShopItem.InAppItemType)
                {
                    _processingPayment = true;
                    OnePF.OpenIAB.purchaseProduct(ShopItem.Sku);
                }
                else
                {
                    Debug.LogError(FailureReason.InValidItem.Details());
                    PurchaseFailedSignal.Dispatch(new FailedPurchaseDetails(ShopItem, FailureReason.InValidItem));
                }

            }
            else
            {
                Debug.LogError(FailureReason.BillingNotSupported.Details());
                PurchaseFailedSignal.Dispatch(new FailedPurchaseDetails(ShopItem, FailureReason.BillingNotSupported));
            }
        }

        #region Billing

        private void OnBillingSupported()
        {
            ISBillingSupported = true;
            Debug.Log("Billing is supported");
            OnePF.OpenIAB.queryInventory(ShopItems.Keys.ToArray());
        }

        private void OnBillingNotSupported(string error)
        {
            ISBillingSupported = false;
            Debug.Log("Billing not supported: " + error);
        }

        private void OnQueryInventorySucceeded(OnePF.Inventory inventory)
        {
            Debug.Log("Query inventory succeeded: " + inventory);

            // Although We keep consuming the consumable expendables immediately. 
            // But what if the internet connection is lost between the purchase and consumption.
            // Therefore Check for delivery of expandable items. 
            // If we own some, we should consume everything immediately

            foreach (string sku in ShopItems.Keys)
            {
                if (ShopItems[sku] == InAppItemType.Consumable)
                {
                    OnePF.Purchase p = inventory.GetPurchase(sku);
                    if (p != null && VerifyPurchase(p))
                    {
                        //Debug.Log("We have MedKit. Consuming it.");
                        OnePF.OpenIAB.consumeProduct(p);
                    }
                }

                //TODO: Else, what do we do with the non-consumable shop items?
            }
        }

        private void OnQueryInventoryFailed(string error)
        {
            Debug.Log("Query inventory failed: " + error);
        }

        bool VerifyPurchase(OnePF.Purchase p)
        {
            VerifyDeveloperPayload(p.DeveloperPayload);
            //TODO: do the signature verification on an external server.
            return true;
        }

        // Verifies the developer payload of a purchase.
        bool VerifyDeveloperPayload(string developerPayload)        
        {
            /*
             * TODO: verify that the developer payload of the purchase is correct. It will be
             * the same one that you sent when initiating the purchase.
             * 
             * WARNING: Locally generating a random string when starting a purchase and 
             * verifying it here might seem like a good approach, but this will fail in the 
             * case where the user purchases an item on one device and then uses your app on 
             * a different device, because on the other device you will not have access to the
             * random string you originally generated.
             *
             * So a good developer payload has these characteristics:
             * 
             * 1. If two different users purchase an item, the payload is different between them,
             *    so that one user's purchase can't be replayed to another user.
             * 
             * 2. The payload must be such that you can verify it even when the app wasn't the
             *    one who initiated the purchase flow (so that items purchased by the user on 
             *    one device work on other devices owned by the user).
             * 
             * Using your own server to store and verify developer payloads across app
             * installations is recommended.
             */
            return true;
        }

        private void OnPurchaseSucceded(OnePF.Purchase purchase)
        {
            Debug.Log("Purchase succeded: " + purchase.Sku + "; Payload: " + purchase.DeveloperPayload);
            if (!VerifyPurchase(purchase))
            {
                return;
            }

            if( ShopItems[purchase.Sku] == InAppItemType.Consumable)
            {
                OnePF.OpenIAB.consumeProduct(purchase);
            }
            else
            {
                PurchaseSuccessfulSignal.Dispatch(new Purchase(purchase));
            }
        }

        private void OnPurchaseFailed(int errorCode, string error)
        {
            Debug.Log("Purchase failed: " + error);
            //PurchaseFailedSignal.Dispatch();

        }

        private void OnConsumePurchaseSucceeded(OnePF.Purchase purchase)
        {
            Debug.Log("Consume purchase succeded: " + purchase.ToString());
            PurchaseSuccessfulSignal.Dispatch(new Purchase(purchase));
        }

        private void OnConsumePurchaseFailed(string error)
        {
            Debug.Log("Consume purchase failed: " + error);
            //TODO: Really? strange why it fails?. However when the game is started again, it should be consumed.
            
        }

        private void OnTransactionRestored(string sku)
        {
            Debug.Log("Transaction restored: " + sku);
        }

        private void OnRestoreSucceeded()
        {
            Debug.Log("Transactions restored successfully");
        }

        private void OnRestoreFailed(string error)
        {
            Debug.Log("Transaction restore failed: " + error);
        }


        #endregion // Billing
    }
}
