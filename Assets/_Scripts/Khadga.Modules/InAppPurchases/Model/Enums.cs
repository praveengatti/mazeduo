﻿namespace Golive.InAppPurchases.Model
{
    public enum InAppItemType
    {
        Consumable,
        NonConsumable
    }

    public enum FailureReason
    {
        BillingNotSupported,        
        InValidItem,
    }
    public static class FailureReasonExtension
    {
        public static string Details(this FailureReason reason)
        {
            switch (reason)
            {
                case FailureReason.BillingNotSupported:
                    return "In app purchases are not working at this moment. Please retry latter.";
                    break;
                case FailureReason.InValidItem:
                    return "This item is not in store. Please update your the app";
                    break;
                default:
                    return "";
            }
        }
    }
}