﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Golive.InAppPurchases.Model
{
    public class FailedPurchaseDetails
    {
        public InAppItem ShopItem { get; private set; }
        public FailureReason FailureReason { get; private set; }
        public FailedPurchaseDetails(InAppItem p, FailureReason reason)
        {
            this.ShopItem = p;
            this.FailureReason = reason;
        }
    }
}
