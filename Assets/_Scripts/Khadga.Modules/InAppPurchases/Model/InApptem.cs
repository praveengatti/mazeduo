﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Golive.InAppPurchases.Model
{
    public class InAppItem
    {
        public string Sku { get; private set; }
        public InAppItemType InAppItemType { get; private set; }

        public InAppItem(string Sku, InAppItemType itemType)
        {
            this.Sku = Sku ?? string.Empty;
            this.InAppItemType = itemType;
        }
    }
}
