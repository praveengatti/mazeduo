﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Golive.InAppPurchases.Model
{
    public class Purchase
    {
        /// <summary>
        /// ITEM_TYPE_INAPP or ITEM_TYPE_SUBS
        /// </summary>
        public string ItemType { get; private set; }
        /// <summary>
        /// A unique order identifier for the transaction. This corresponds to the Google Wallet Order ID.
        /// </summary>
        public string OrderId { get; private set; }
        /// <summary>
        /// The application package from which the purchase originated.
        /// </summary>
        public string PackageName { get; private set; }
        /// <summary>
        /// The item's product identifier. Every item has a product ID, which you must specify in the application's product list on the Google Play Developer Console.
        /// </summary>
        public string Sku { get; private set; }
        /// <summary>
        /// The time the product was purchased, in milliseconds since the epoch (Jan 1, 1970).
        /// </summary>
        public long PurchaseTime { get; private set; }
        /// <summary>
        /// The purchase state of the order. Possible values are 0 (purchased), 1 (canceled), or 2 (refunded).
        /// </summary>
        public int PurchaseState { get; private set; }
        /// <summary>
        /// A developer-specified string that contains supplemental information about an order. You can specify a value for this field when you make a getBuyIntent request.
        /// </summary>
        public string DeveloperPayload { get; private set; }
        /// <summary>
        /// A token that uniquely identifies a purchase for a given item and user pair. 
        /// </summary>
        public string Token { get; private set; }
        /// <summary>
        /// JSON sent by the current store
        /// </summary>
        public string OriginalJson { get; private set; }
        /// <summary>
        /// Signature of the JSON string
        /// </summary>
        public string Signature { get; private set; }
        /// <summary>
        /// Current store name
        /// </summary>
        public string AppstoreName { get; private set; }
        /// <summary>
        /// Purchase Receipt of the order (iOS only)
        /// </summary>
        public string Receipt { get; private set; }

        internal Purchase(OnePF.Purchase p)
        {
            ItemType = p.ItemType;
            OrderId = p.OrderId;
            PackageName = p.PackageName;
            Sku = p.Sku;
            PurchaseTime = p.PurchaseTime;
            PurchaseState = p.PurchaseState;
            DeveloperPayload = p.DeveloperPayload;
            Token = p.Token;
            Signature = p.Signature;
            AppstoreName = p.AppstoreName;
            Receipt = p.Receipt;
        }
    }
}
