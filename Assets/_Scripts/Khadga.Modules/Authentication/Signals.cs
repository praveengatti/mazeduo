﻿using strange.extensions.signal.impl;

namespace Khadga.Authentication
{
    public class TryLoginSignal : Signal<Credentials> { } 

    public class TryRegisterSignal: Signal<Credentials> { }
}