﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Authentication
{
    public interface IAuthenticationService
    {
        bool IsAuthenticated { get; }
        bool IsBaasAvailable();

        void TryToLogin(Credentials credentials);
        void TryToRegister(Credentials credentials);

        void InitializeAndAuthenticate();
        void GetAccoutDetails();
        void ListGameFriends();
        void InitializeFBConnectivity();

    }
}























