﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Authentication
{
    public class TryLoginCommand : Command
    {
        [Inject]
        public Credentials Credentials { get; set; }

        [Inject]
        public IAuthenticationService AuthenticationService { get; set; }

        public override void Execute()
        {
            AuthenticationService.TryToLogin(Credentials);
        }
    }
}










