﻿using strange.extensions.command.impl;


namespace Khadga.Authentication
{
    public class TryRegistrationCommand : Command
    {
        [Inject]
        public Credentials Credentials { get; set; }

        [Inject]
        public IAuthenticationService AuthenticationService { get; set; }

        public override void Execute()
        {
            AuthenticationService.TryToRegister(Credentials);
        }
    }
}