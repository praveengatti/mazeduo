﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Shared
{
    [RequireComponent(typeof(CanvasGroup))]
    public class UIView : View
    {
        private CanvasGroup CanvasGroup { get; set; }

        public void Hide()
        {
            SetStateOfCanvasGroup(0);
        }


        public void Show()
        {
            SetStateOfCanvasGroup(1);
        }


        private void SetStateOfCanvasGroup(float alpha)
        {
            CanvasGroup = GetComponent<CanvasGroup>();
            CanvasGroup.alpha = alpha;
            CanvasGroup.interactable = alpha > 0;
            CanvasGroup.blocksRaycasts = alpha > 0;
        }

    }
}


