﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Khadga.Shared
{
    public class Toast : UIView
    {
        public Text Message;

        public float DisableIn;

        public void Initialize()
        {
            Show();
            Invoke("Hide", DisableIn);
        }
    }
}