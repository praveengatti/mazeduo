﻿using UnityEngine;
using Newtonsoft.Json;
using System.IO;
namespace Khadga.Utility.LocalData
{
    public static class LocalDataUtil
    {
        public static void SaveToLocal<T>(T obj, string fileName)
        {
            if (obj == null)
            {
                return;
            }

            var json = JsonConvert.SerializeObject(obj);
            File.WriteAllText(Application.persistentDataPath + "/" + fileName, json);
            Debug.Log("file name: " + fileName + " saved at " + Application.persistentDataPath + "/" + fileName);
        }

        public static T GetLocalDataOfType<T>(string fileName)
        {
            string filePath = Application.persistentDataPath + "/" + fileName;
            string json = File.ReadAllText(filePath);
            var variable = JsonConvert.DeserializeObject<T>(json);
            return variable;

        }

    }
}