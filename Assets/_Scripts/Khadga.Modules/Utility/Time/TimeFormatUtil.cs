﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Utility.Time
{
    public static class TimeFormatUtil
    {
        static int HOUR_SECONDS = 60 * 60;
        static int MINUTE_SECONDS = 60;
        static int DAY_SECONDS = 24 * HOUR_SECONDS;
        static int SECOND_MILLISECONDS = 1000;

        public static string SecondToHHMMSSmm(float seconds)
        {
            int sec = (int)Mathf.Floor(seconds);

            if (seconds >= HOUR_SECONDS)
            {
                return (sec / HOUR_SECONDS).ToString() + "h :" +
                    (sec - HOUR_SECONDS) / MINUTE_SECONDS + "m :" +
                    sec % MINUTE_SECONDS + "s :" +
                    (int)(seconds * SECOND_MILLISECONDS % SECOND_MILLISECONDS) + "ms";
            }
            else
            {
                return (sec) / MINUTE_SECONDS + "m :" +
                    sec % MINUTE_SECONDS + "s :" +
                    (int)(seconds * SECOND_MILLISECONDS % SECOND_MILLISECONDS) + "ms";
            }
        }

        public static long SecFloatToLong(float seconds)
        {
            return (long)(seconds * SECOND_MILLISECONDS);
        }


    }
}