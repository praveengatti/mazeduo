﻿
﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;

namespace Khadga.Utility
{
    public class ShareScreenshot : MonoBehaviour
    {

        private bool isProcessing = false;

        public void shareScreenshot()
        {

            if (!isProcessing /*&& GameObject.Find("Ask Friend").GetComponent<Image>().enabled*/)
                StartCoroutine(captureScreenshot());
        }

        public IEnumerator captureScreenshot()
        {
            isProcessing = true;
            yield return new WaitForEndOfFrame();

            Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);

            // put buffer into texture
            //screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height),0,0);
            //create a Rect object as per your needs.
            screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);

            // apply
            screenTexture.Apply();

            //----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- PHOTO

            //byte[] dataToSave = Resources.Load<TextAsset>("everton").bytes;
            byte[] dataToSave = screenTexture.EncodeToPNG();

            string destination = Path.Combine(Application.persistentDataPath, System.DateTime.Now.ToString("yyyy-MM-dd-HHmmss") + ".png");

            File.WriteAllBytes(destination, dataToSave);


            if (!Application.isEditor)
            {
                // block to open the file and share it ------------START
                AndroidJavaClass intentClass = new AndroidJavaClass("android.content.Intent");
                AndroidJavaObject intentObject = new AndroidJavaObject("android.content.Intent");
                intentObject.Call<AndroidJavaObject>("setAction", intentClass.GetStatic<string>("ACTION_SEND"));
                AndroidJavaClass uriClass = new AndroidJavaClass("android.net.Uri");
                AndroidJavaObject uriObject = uriClass.CallStatic<AndroidJavaObject>("parse", "file://" + destination);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_STREAM"), uriObject);
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_TEXT"), "Can you beat my time record?\n" +
                                                     "Download the game on play store at " + "\nhttps://play.google.com");
                intentObject.Call<AndroidJavaObject>("putExtra", intentClass.GetStatic<string>("EXTRA_SUBJECT"), "?");
                intentObject.Call<AndroidJavaObject>("setType", "image/jpeg");
                AndroidJavaClass unity = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                AndroidJavaObject currentActivity = unity.GetStatic<AndroidJavaObject>("currentActivity");

                // option one WITHOUT chooser:
                currentActivity.Call("startActivity", intentObject);

                // block to open the file and share it ------------END

            }
            isProcessing = false;

        }
    }
}