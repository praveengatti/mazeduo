﻿using UnityEngine;
using System.Collections;

namespace Khadga.Utility.Action
{

    public class DragAndDrop : MonoBehaviour
    {

        public Camera Camera;
        private bool IsDragging;
        private Rigidbody RB;
        private Collider Collider;
        void Start()
        {
            RB = GetComponent<Rigidbody>();
            Collider = GetComponent<Collider>();
        }

        void LateUpdate()
        {
            // This draws a line (ray) from the camera at a distance of 3
            // If it hits something, it moves the object to that point, if not, it moves it to the end of the ray (3 units out)
            if (IsDragging)
            {
                Ray ray = Camera.ScreenPointToRay(Input.mousePosition);

                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, 3f))
                {
                    transform.position = hit.point;
                }
                else
                {
                    transform.position = ray.GetPoint(3f);
                }
            }
        }

        void OnMouseDown()
        {
            IsDragging = true;
            RB.isKinematic = true;
            Collider.enabled = false;
        }

        // Adds the force when you let go based on the Mouse X/Y values.
        void OnMouseUp()
        {
            IsDragging = false;
            RB.isKinematic = false;
            Collider.enabled = true;

            RB.AddForce(Camera.transform.right * Input.GetAxis("Mouse X") * 10f, ForceMode.Impulse);
            RB.AddForce(Camera.transform.up * Input.GetAxis("Mouse Y") * 10f, ForceMode.Impulse);
        }

    }
}