﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Leaderboard
{
    public interface ILeaderboardService
    {
        void SubmitMultiplayerLeaderboard(float TimeTaken, string pair);
        void RequestLeaderboard();
        void RequestMultiplayerLeaderboard();
    }
}