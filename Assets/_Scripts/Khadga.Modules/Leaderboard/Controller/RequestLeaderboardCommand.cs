﻿using strange.extensions.command.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Leaderboard
{
    public class RequestLeaderboardCommand : Command
    {

        [Inject]
        public ILeaderboardService LeaderboardService { get; set; }

        public override void Execute()
        {
            LeaderboardService.RequestLeaderboard();
            LeaderboardService.RequestMultiplayerLeaderboard();
        }
    }
}