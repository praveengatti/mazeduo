﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Leaderboard
{
    public class LeaderboardItem
    {
        public string Name;
        public int Rank;
        public string DecisionAttribute1;
        public string DecisionAttribute2;
    }
}