﻿using System;

namespace Khadga.Shared
{
    public interface IPlayer
    {
        string Name { get; set; }
        string NickName { get; set; }
        bool IsFBLoggedIn { get; set; }
        bool CanReadTimerValue { get; set; }
        float RechargeTimer { get; set; }
        int XP { get; set; }
        int Level { get; set; }
        bool IsPremiumUser { get; set; }
        int Wins { get; set; }
        int Losses { get; set; } 
        int Draws { get; set; }
        int WinStreak { get; set; }
        int LoseStreak { get; set; }
        int BestWinStreak { get; set; }
        float BestTime { get; set; }
        int NumberOfMagesSolved { get; set; }
        int NumberOfChargesLeft { get; set; }
        long LastActivity { get; set; }

        float MultiplayerBestTime { get; set; }
        string BestPair { get; set; }

    }
}





