﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Khadga.Shared
{

    // the base class for all the objects which player interacts or controls in game
    public class Entity
    {
        public string Name;
        public string ShortDescription;
        public string LongDescription;
        public float Attack;
        public float Health;
        public float Armour;
        public float Speed;
        public float Range;
        public int Level;

    }
}