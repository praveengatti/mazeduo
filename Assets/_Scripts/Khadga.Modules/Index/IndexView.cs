﻿using strange.extensions.mediation.impl;
using strange.extensions.signal.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Khadga.Index
{
    public class IndexView : View
    {
        internal Signal PersistDataSignal { get; set; }

        public void Initialize()
        {
            PersistDataSignal = new Signal();
        }

        public void OnApplicationQuit()
        {
            PersistDataSignal.Dispatch();
        }

        public void OnApplicationPause()
        {
            PersistDataSignal.Dispatch();
        }

        public void OnApplicationFocus(bool focus)
        {
            if (!focus)
            {
                PersistDataSignal.Dispatch();
            }
        }
    }
}















