﻿using strange.extensions.mediation.impl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Khadga.Index
{
    public class IndexMediator : Mediator
    {
        [Inject]
        public IndexView IndexView { get; set; }

        [Inject]
        public PersistDataSignal PersistDataSignal { get; set; }


        public override void OnRegister()
        {
            IndexView.Initialize();
            IndexView.PersistDataSignal.AddListener(PersistData);
        }

        public override void OnRemove()
        {
            IndexView.PersistDataSignal.RemoveListener(PersistData);
        }

        private void PersistData()
        {
            PersistDataSignal.Dispatch();
        }
    }
}