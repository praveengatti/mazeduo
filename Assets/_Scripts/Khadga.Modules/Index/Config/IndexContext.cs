﻿using Khadga.Authentication;
using Khadga.Fusion;
using Khadga.Fusion.Controller;
using Khadga.Fusion.Model;
using Khadga.Fusion.Service;
using Khadga.Fusion.Views;
using Khadga.GSBaas;
using Khadga.GSBass;
using Khadga.Leaderboard;
using Khadga.Shared;
using strange.extensions.context.impl;
using UnityEngine.SceneManagement;

namespace Khadga.Index
{
    public class IndexContext : SignalContext
    {
        public IndexContext(ContextView contextView) : base(contextView)
        {
        }

        protected override void mapBindings()
        {

            mediationBinder.Bind<PVPView>().To<PVPMediator>();
            mediationBinder.Bind<PVPPlayerView>().To<PVPPlayerMediator>();
            injectionBinder.Bind<ChallengeDetails>().ToValue(new ChallengeDetails()).ToSingleton().CrossContext();
            injectionBinder.Bind<PVPGameEndSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<OpponentRTSignal>().ToSingleton().CrossContext();

            #region LEADERBOARD
            injectionBinder.Bind<ILeaderboardService>().To<GSLeaderboardService>().ToSingleton().CrossContext();

            injectionBinder.Bind<RequestLeaderboardSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<LeaderboardResponseSignal>().ToSingleton().CrossContext();
            commandBinder.Bind<RequestLeaderboardSignal>().To<RequestLeaderboardCommand>();
            injectionBinder.Bind<MultiplayerLeaderboarResponseSignal>().ToSingleton().CrossContext();
            #endregion

            #region Authentication
            injectionBinder.Bind<IAuthenticationService>().To<GSAuthentication>().ToSingleton();

            #endregion



            #region PREGAME
            injectionBinder.Bind<ConnectWithFBSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GetPlayerSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<DataService>().ToSingleton();
            injectionBinder.Bind<UpdatePlayerInLocalSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UpdatePlayerInRemoteSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<UpdateTimerSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FBLoggedInSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FBAuthenticatedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<FBFriendsListResponseSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ChallengeIssuedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ChallengeStartedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ChallengeAcceptedSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<ChallengeFriendSignal>().ToSingleton().CrossContext();
            injectionBinder.Bind<GetAccountDetailsSignal>().ToSingleton().CrossContext();


            injectionBinder.Bind<GSRealTimeService>().ToSingleton().CrossContext();
            commandBinder.Bind<GetAccountDetailsSignal>().To<GetAccountDetailsCommand>().Pooled();
            commandBinder.Bind<ChallengeAcceptedSignal>().To<RequestMatchCommand>();
            commandBinder.Bind<ChallengeFriendSignal>().To<ChallengeFriendCommand>();
            commandBinder.Bind<FBAuthenticatedSignal>().To<FBLoggedInCommand>();
            commandBinder.Bind<ConnectWithFBSignal>().To<ConnectWithFBCommand>();
            commandBinder.Bind<UpdateTimerSignal>().To<UpdateRechargeTimeCommand>();
            commandBinder.Bind<UpdatePlayerInLocalSignal>().To<UpdatePlayerInLocalCommand>();
            commandBinder.Bind<UpdatePlayerInRemoteSignal>().To<UpdatePlayerInRemoteCommand>();
            commandBinder.Bind<StartMenuSignal>().To<StartMenuCommand>();
            commandBinder.Bind<GetPlayerSignal>().To<GetPlayerCommand>();
            commandBinder.Bind<LoadPlayerSignal>().To<LoadPlayerCommand>();

            commandBinder.Bind<StartIndexSignal>().InSequence().
                To<AuthenticateCommand>().To<InitializeInviteCommand>();
                //To<StartMenuCommand>();
      //      commandBinder.Bind<StartMainSignal>().InSequence().
      //To<RegisterWithGCMCommand>().
      //To<LoadConfigurationCommand>().
      //To<LoadCharacterCollectionsCommand>().
      //To<LoadCoinPackCommand>().
      //To<LoadPotionCommand>().
      //To<LoadPlayersCommand>().
      //To<LoadLevelUpConfigCommand>().
      //To<HealWithTimeCommand>().
      //To<BindQuestsCommand>().
      //To<LoadTutorialDataCommand>().
      //To<LoadAchievementsDataCommand>().
      //To<BindArenaCommand>().
      //To<StartMainCommand>().Once();
            #endregion
        }

        public override void Launch()
        {
            injectionBinder.GetInstance<StartIndexSignal>().Dispatch();
        }
    }
}



