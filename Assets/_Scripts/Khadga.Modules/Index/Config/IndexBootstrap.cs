﻿using Assets.SimpleAndroidNotifications;
using strange.extensions.context.impl;
using System;
using UnityEngine;

namespace Khadga.Index
{
    public class IndexBootstrap : ContextView
    {

        private void Awake()
        {

            context = new IndexContext(this);

            DontDestroyOnLoad(this);
                
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
        }
    }

}








