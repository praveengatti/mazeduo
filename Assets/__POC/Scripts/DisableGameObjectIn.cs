﻿using UnityEngine;
using System.Collections;

public class DisableGameObjectIn : MonoBehaviour
{
	public float Sec;
	// Use this for initialization
	void Start ()
	{
		StartCoroutine ("DisableIn", Sec);
	}
	
	public IEnumerator DisableIn (float sec)
	{
		yield return new WaitForSeconds (sec);
		this.gameObject.SetActive (false);
	}
}
