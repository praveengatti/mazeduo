﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuGameController : MonoBehaviour {

    public GameObject Ball1;
    public GameObject Ball2;
    public Light Ball1Light;
    public Light Ball2Light;
    public int LightRange;

    Vector3 Position;
    Vector3 PreviousPosition;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player2" && this.gameObject.tag == "Player")
        {
            Ball1.transform.position = SetRandomPosition();
            PreviousPosition = Ball1.transform.position;
            Ball2.transform.position = SetRandomPosition();
            Ball1Light.intensity = 0;
            Ball2Light.intensity = 0;
        }
    }

    private void FixedUpdate()
    {
        if(Ball1Light.intensity <LightRange)
        { Ball1Light.intensity += Time.deltaTime; }

        if (Ball2Light.intensity < LightRange)
        { Ball2Light.intensity += Time.deltaTime; }
    }

    Vector3 SetRandomPosition()
    {

        Position.x = 2 * Random.Range(1, 15) - 1;
        Position.y = 0.5f;
        Position.z = 2 * Random.Range(1, 7) - 1;
        if(Vector3.Distance(Position,PreviousPosition)<5)
        {
            Position = SetRandomPosition();
        }
        return Position;
    }
}
