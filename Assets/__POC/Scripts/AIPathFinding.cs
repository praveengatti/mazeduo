﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AIPathFinding : MonoBehaviour {

    public Transform Goal;

    NavMeshAgent Agent;
    // Use this for initialization
    void Start () {
       Agent  = GetComponent<NavMeshAgent>();

    }

    // Update is called once per frame
    void Update () {
        Agent.destination = Goal.position;
    }
}
