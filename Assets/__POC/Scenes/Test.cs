﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Khadga.Utility;
using Khadga.Utility.LocalData;

public class Test : MonoBehaviour {

    int[,] Maze;
	// Use this for initialization
	void Start () {
        Maze = MazeUtil.CreateMaze(33 ,15);

        LocalDataUtil.SaveToLocal<int[,]>(Maze, "StaticMaze");

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
